const express=require("express");
const app=express();
const morgan = require('morgan');
const {checkPsswrd, validData} = require('./middleware');
const {writeFile, readFile, getFiles, deleteFile} = require('./controller');


app.use(express.json());
app.use(morgan('tiny'));


app.post('/api/files', checkPsswrd, validData, writeFile);
app.get('/api/files', getFiles);
app.delete('/api/files/:filename', deleteFile);
app.get(['/api/files/:filename'], readFile);


app.listen(8080, () => {console.log('Server started on port 8080')});
