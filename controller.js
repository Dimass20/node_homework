const fs = require("fs");
const path = require('path');


exports.writeFile=function (req, res){
  let psswrd=req.query.password;
  psswrd = psswrd == undefined ? null : psswrd;
  let filename = req.body.filename;
  let content = req.body.content;
  let users = fs.readFileSync("files.json", "utf8");
  users =  JSON.parse(users);
   

  fs.writeFile(`./files/${filename}`, content, (error) => {
    if(error){
      return res.status(500)
      .json({
        message:"Server error"
      }); 
    }

    users.push({
      "id":Number(new Date()),
      "nameFile": filename,
      "psswrd": psswrd,
      "date" : new Date()
    });

    users = JSON.stringify(users);
    fs.writeFileSync("files.json", users, (err) => {
      return res.status(500).json({
        message:"Server error"
      });
    });
    return res.status(200)
    .json({
      message:"File created successfully"
    });
  });
}


exports.readFile = function(req, res){
  let pswrd = req.query.password;
  pswrd = pswrd == undefined ? null : pswrd;
  let fileName=req.params['filename'];
  let users = fs.readFileSync("files.json", "utf8");
  users = JSON.parse(users);

  let file = users.find((el, key) => {
    if(el.nameFile === fileName) return true;
  })

  if (file == undefined){
    return res.status(400)
    .json({
      message:`No file with ${fileName} filename found`
    }); 
  }
  
  if (pswrd !== file.psswrd){
    return res.status(400)
    .json({
      message:`Please specify password parameter`
    }); 
  }

  let content = fs.readFileSync(`./files/${fileName}`, "utf8", (e) => {
    return res.status(500)
    .json({
      "message": "Server error"
    }); 
  });

  let stats = fs.statSync(`./files/${fileName}`);
  return res
    .json({
      "message": "Success",
      "filename": fileName,
      "content": content,
      "extension": path.extname(fileName).substr(1),
      "uploadedDate": stats.mtime
    }); 
  
}

exports.getFiles=function(req, res){
  fs.readdirSync('./files/').forEach(file => {
    console.log(file);
  });
  try {
    let files = fs.readdirSync('./files/');
    return res.status(200)
      .json({
        "message": "Success",
        "files": [
          ...files
        ]
      }); 
  } catch (err) {
    return res.status(500)
    .json({
      "message": "Server error"
    }); 
  }
}

exports.deleteFile=function(req, res) {
  let fileName = req.params['filename'];

  if (fileName == undefined){
    return res.status(400)
    .json({
      message:`No file with ${fileName} filename found`
    }); 
  }

  fs.unlink(`./files/${fileName}`, (err) => {
    if(err && err.code == 'ENOENT') {
      return res.status(400)
      .json({
        message:`File doesn't exist, won't remove it.`
      }); 
    } else if (err) {
      return res.status(500)
      .json({
        "message": "Server error"
      }); 
    } else {
      removeFile(fileName);
      return res.status(200)
      .json({
        "message": "The file was removed"
      }); 
    }
});

}


function removeFile(file){
  let files = fs.readFileSync("files.json", "utf8");
  files = JSON.parse(files);
  let index = files.findIndex((user) => {
    if (user.nameFile === file) return true;
  })
  files.splice(index, 1);
  files = JSON.stringify(files)
  fs.writeFileSync("files.json", files);
}