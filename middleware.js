exports.checkPsswrd = function (req, res, next){
  let psswrd = req.query.password;

  if(typeof psswrd != 'undefined' && psswrd.length<1){
    return res.status(400)
    .json({
      message:`Please specify password parameter`
    }); 
  }
  next();
}

exports.validData = function (req, res, next){
  let arrFile=['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
  let filename = req.body.filename;
  let content = req.body.content;

  if(filename == null){
    return res.status(400)
    .json({
      message:`Please specify filename parameter`
    }); 
  }

  let valFile=arrFile.find((el, key) => {
    if(filename.indexOf(el) !== -1){
      return true;
    }
  })

  if(valFile == undefined){
    return res.status(400)
    .json({
      message:`Please specify filename parameter`
    }); 
  }

  if(content == null){
    return res.status(400)
    .json({
      message:`Please specify 'content' parameter`
    }); 
  }

  next();
}

